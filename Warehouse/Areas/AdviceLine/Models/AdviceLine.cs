﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Warehouse.Models;

namespace Warehouse.Areas.AdviceLine.Models
{
    public class AdviceLine : EntityBase
    {
        public string TicketId => $"Wire{Id}";

        public string ConsultantId { get; set; }
        public virtual ApplicationUser Consultant { get; set; }

        [Required]
        public string RaisedById { get; set; }
        public virtual ApplicationUser RaisedBy { get; set; }

        [MaxLength(1000)]
        public string Title { get; set; }
        [MaxLength(50000)]
        public string Description { get; set; }
        [MaxLength(50000)]
        public string PrivateNotes { get; set; }
        public string Risk { get; set; }
        public string RaisedMethod { get; set; }

        //type
        //status
        //insurance
    }
}