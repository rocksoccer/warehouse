﻿var defaultApp = getDefaultApp();

defaultApp.directive("fileuploadOnChange", function () {
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
            var onChangeFunc = scope.$eval(attrs.fileuploadOnChange);
            element.bind("change", onChangeFunc);
        }
    };
});

defaultApp.controller("AttachmentUploadController", ["$scope", "$attrs", "$element", function ($scope, $attrs, $element) {
    var attachmentId = parseInt($attrs.initialAttachmentId);
    if (isNaN(attachmentId)) {
        attachmentId = -1;
    }
    var initialAttachmentName = $attrs.initialAttachmentName;
    if (initialAttachmentName === null || initialAttachmentName === undefined || initialAttachmentName.trim() === "") {
        initialAttachmentName = null;
    }
    var attachmentsPath = $attrs.attachmentsPath;

    var fileUpload = $element.find("input:file")[0];
    var getFile = function () { return fileUpload.files[0]; };

    var postFileContent = function (url) {
        var file = getFile();
        $.ajax({
            url: url,
            data: file,
            type: "PUT",
            dataType: "xml",
            contentType: file.type,
            processData: false,
            success: function (data, textStatus, jqXHR) {
                $scope.FileUploaded = true;
                $scope.$digest();
                initialAttachmentName = file.name;
                $(fileUpload).trigger("attachment:created", [attachmentId]);
                $(fileUpload).val(null); // to avoid the file to be sent to server again
            },
            error: function (jqXHR) {
                debugger;
                //if (options.error) {
                //    var errorKey = $(jqXHR.responseText).find('Code').text();
                //    options.error(jqXHR, errorKey);
                //}
            }
        });
    };

    $scope.FileUploaded = attachmentId > 0;
    $scope.Filename = function () {
        var file = getFile();
        return initialAttachmentName || (file && file.name);
    };
    $scope.AttachmentPath = function () { return attachmentsPath + "/" + attachmentId; };

    $scope.OnFileUploadChange = function (event) {
        initialAttachmentName = null;
        var file = getFile();
        var fileInfo = { Filename: file.name, Filesize: file.size, MimeType: file.type };
        $.post(attachmentsPath, fileInfo, function (data) {
            attachmentId = data.attachmentId;
            postFileContent(data.url);
        });
    };

    $scope.DeleteFile = function () {
        var deletedAttachmentId = attachmentId;
        attachmentId = -1;
        $scope.FileUploaded = false;

        $(fileUpload).trigger("attachment:deleted", [deletedAttachmentId]);
    };
}]);
