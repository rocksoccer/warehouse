﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Warehouse.Areas.Compliance.Libs;
using Warehouse.Areas.Compliance.Models;
using Warehouse.Areas.Compliance.Services;
using Warehouse.Libraries.UsersManagement;
using Warehouse.Services;

namespace Warehouse.Areas.Compliance.Controllers
{
    [Authorize(Roles = ApplicationRole.Admin)]
    public class TasksController : Controller
    {
        private readonly ITaskService _taskService;
        private readonly IUserService _userService;
        private readonly IJobService _jobService;

        public TasksController(ITaskService taskService, IJobService jobService, IUserService userService)
        {
            _taskService = taskService;
            _userService = userService;
            _jobService = jobService;
        }

        public ActionResult Index(int? page)
        {
            var tasks = _taskService.Tasks();
            var pageNumber = page ?? 1;
            var pagedTasks = tasks.ToPagedList(pageNumber, PageSize);
            return View(pagedTasks);
        }

        public ActionResult New()
        {
            PopulateUsers();
            return View();
        }

        public ActionResult Create(Task task, string scheduleType)
        {
            var schedule = CreateSchedule(scheduleType);
            task.Schedule = schedule;

            //TODO: need transaction to protect creation of task and job

            var createdTask = _taskService.AddTask(task);
            _jobService.CreateJobForTask(createdTask);
            
            return RedirectToAction("Index");
        }

        public ActionResult Show(int id)
        {
            return RedirectToAction("Edit");
        }

        public ActionResult Edit(int id)
        {
            PopulateUsers();
            var task = _taskService.GetTask(id);
            return View("New", task);
        }

        public ActionResult Update(Task task, string scheduleType)
        {
            var schedule = CreateSchedule(scheduleType);
            task.Schedule = schedule;

            bool isScheduleUpdated;
            var updatedTask = _taskService.UpdateTask(task, out isScheduleUpdated);
            if (isScheduleUpdated)
            {
                _jobService.ClearJobsForTask(updatedTask);
                _jobService.CreateJobForTask(updatedTask);
            }

            return RedirectToAction("Show");
        }

        private Schedule CreateSchedule(string scheduleType)
        {
            var type = ParseScheduleType(scheduleType);

            var schedule = new Schedule();
            switch (type)
            {
                case ScheduleTypes.Once:
                    var runAt = DateTime.Parse(Request.Params["scheduleRunAt"]);
                    schedule.CreateOnceSchedule(runAt);
                    break;
                case ScheduleTypes.Repeative:
                    var start = DateTime.Parse(Request.Params["scheduleStart"]);
                    var end = DateTime.Parse(Request.Params["scheduleEnd"]);
                    var cron = Request.Params["scheduleCron"];
                    schedule.CreateRepeativeSchedule(start, end, cron);
                    break;
            }

            return schedule;
        }

        private void PopulateUsers()
        {
            var users = _userService.Users();
            ViewBag.users = new SelectList(users, "Id", "Fullname");
        }

        private ScheduleTypes ParseScheduleType(string scheduleType)
        {
            var documentType = (ScheduleTypes)Enum.Parse(typeof(ScheduleTypes), scheduleType, true);
            return documentType;
        }

        private int PageSize => Convert.ToInt32(ConfigurationManager.AppSettings["ListingPageSize"]);
    }
}