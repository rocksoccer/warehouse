﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Microsoft.Practices.Unity;
using Quartz;
using Warehouse.App_Start;
using Warehouse.Areas.Compliance.Models;
using Warehouse.Areas.EmailCenter.Services;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Areas.Compliance.Jobs
{
    public class TaskNotificationJob : IJob
    {
        private enum JobType
        {
            Due,
            Expired
        }

        private struct JobInfo
        {
            public string Title;
            public string Type;
            public string Description;
            public DateTime RunAt;
        }

        private struct Jobs
        {
            public Jobs(string userEmail)
            {
                UserEmail = userEmail;
                DueJobs = new List<JobInfo>();
                ExpiredJobs = new List<JobInfo>();
            }

            public readonly string UserEmail;
            public readonly List<JobInfo> DueJobs;
            public readonly List<JobInfo> ExpiredJobs;
        }

        public TaskNotificationJob()
        {
            IUnityContainer unityContainer = UnityConfig.GetConfiguredContainer();
            _emailService = unityContainer.Resolve<IEmailService>();
        }

        private readonly IEmailService _emailService;

        public void Execute(IJobExecutionContext context)
        {
            var dayOffset = Convert.ToInt32(ConfigurationManager.AppSettings["TaskNotificationJobDayOffset"]);
            var dueJobDate = DateTime.Today.AddDays(dayOffset);
            var appContext = new AppContext();
            var jobs = new Dictionary<string, Jobs>();

            var dueJobs = appContext.Context.Jobs.Where(job => DbFunctions.TruncateTime(job.RunAt) == dueJobDate);
            var expiredJobs = appContext.Context.Jobs.Where(job => DbFunctions.TruncateTime(job.RunAt) < dueJobDate);

            AddJobs(dueJobs, jobs, JobType.Due);
            AddJobs(expiredJobs, jobs, JobType.Expired);

            foreach (var pair in jobs)
            {
                SendEmail(pair.Key, pair.Value, dueJobDate);
            }
        }

        private void AddJobs(IQueryable<Job> jobsToAdd, Dictionary<string, Jobs> groupedJobs, JobType type)
        {
            var jobs = jobsToAdd.Select(job => new
            {
                job.Task.ResponsibleUserId,
                job.Task.ResponsibleUser.Email,
                job.RunAt,
                job.Task.Title,
                job.Task.Type,
                job.Task.Description
            });

            foreach (var job in jobs)
            {
                AddJobForUser(groupedJobs,
                    job.ResponsibleUserId,
                    job.Email,
                    new JobInfo { Title = job.Title, Type = job.Type, Description = job.Description, RunAt = job.RunAt },
                    type
                    );
            }
        }

        private void AddJobForUser(Dictionary<string, Jobs> jobs, string userId, string email, JobInfo jobInfo, JobType type)
        {
            if (!jobs.ContainsKey(userId))
            {
                jobs[userId] = new Jobs(email);
            }

            switch (type)
            {
                case JobType.Due:
                    jobs[userId].DueJobs.Add(jobInfo);
                    break;
                case JobType.Expired:
                    jobs[userId].ExpiredJobs.Add(jobInfo);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        private void SendEmail(string userId, Jobs jobs, DateTime date)
        {
            var rowTemplate = GetEmailTemplate("Row");
            var dueJobRows = BuildJobRows(jobs.DueJobs, rowTemplate);
            var expiredJobsRows = BuildJobRows(jobs.ExpiredJobs, rowTemplate);

            var bodyTemplate = GetEmailTemplate("Body");
            var body = string.Format(bodyTemplate, dueJobRows, expiredJobsRows);

            var subject = $"Compliance events for {date}";
            _emailService.SendEmail(jobs.UserEmail, subject, body);
        }

        private string BuildJobRows(List<JobInfo> jobs, string template)
        {
            if (!jobs.Any())
            {
                return string.Format(template, "No jobs", string.Empty, string.Empty, string.Empty);
            }

            var rows = new StringBuilder();
            foreach (var jobInfo in jobs)
            {
                var row = string.Format(template, jobInfo.RunAt, jobInfo.Title, jobInfo.Type, jobInfo.Description);
                rows.AppendLine(row);
            }
            return rows.ToString();
        }

        private string GetEmailTemplate(string name)
        {
            return File.ReadAllText($"{HttpRuntime.AppDomainAppPath}/Areas/Compliance/Config/TaskNotificationJobEmail{name}.html");
        }
    }
}