﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Warehouse.Areas.Compliance.Libs;
using Warehouse.Models;

namespace Warehouse.Areas.Compliance.Models
{
    public class Task : EntityBase
    {
        [Required]
        public string ResponsibleUserId { get; set; }
        public virtual ApplicationUser ResponsibleUser { get; set; }

        [Required]
        public string Title { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public Schedule Schedule { get; set; }

        public bool Completed { get; set; }

        public virtual ICollection<Job> Job { get; set; }
    }
}