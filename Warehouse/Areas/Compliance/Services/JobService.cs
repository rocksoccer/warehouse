﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using NCrontab;
using Warehouse.Areas.Compliance.Libs;
using Warehouse.Areas.Compliance.Models;
using Warehouse.Libraries;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Areas.Compliance.Services
{
    public interface IJobService
    {
        Job CreateJobForTask(Task task);
        Job ClearJobsForTask(Task task);
        Job ClearJobsForTask(int taskId);
    }

    public class JobService : IJobService
    {
        public Job CreateJobForTask(Task task)
        {
            var schedule = task.Schedule;
            var nextOccurrence = schedule.GetNextOccurrence();
            if (!nextOccurrence.HasValue) return null;

            var job = new Job
            {
                TaskId = task.Id,
                State = "Awaiting",
                RunAt = nextOccurrence.Value
            };
            var appContext = new AppContext();
            appContext.Context.Jobs.Add(job);
            appContext.Context.SaveChanges();
            return job;
        }

        public Job ClearJobsForTask(Task task)
        {
            return ClearJobsForTask(task.Id);
        }

        public Job ClearJobsForTask(int taskId)
        {
            var appContext = new AppContext();
            var jobs = appContext.Context.Jobs.Where(j => j.TaskId == taskId);
            appContext.Context.Jobs.RemoveRange(jobs);
            appContext.Context.SaveChanges();
            return jobs.FirstOrDefault();
        }
    }
}