﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Areas.Compliance.Models;
using Warehouse.Libraries;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Areas.Compliance.Services
{
    public interface ITaskService
    {
        List<Task> Tasks();
        Task AddTask(Task task);
        Task GetTask(int taskId);
        Task UpdateTask(Task task, out bool isScheduleUpdated);
    }

    public class TaskService : ITaskService
    {
        public List<Task> Tasks()
        {
            var appContext = new AppContext();
            return appContext.Context.Tasks.ToList();
        }

        public Task AddTask(Task task)
        {
            var appContext = new AppContext();
            var created = appContext.Context.Tasks.Add(task);
            appContext.Context.SaveChanges();
            return created;
        }

        public Task GetTask(int taskId)
        {
            var appContext = new AppContext();
            var task = appContext.Context.Tasks.Find(taskId);
            return task;
        }

        public Task UpdateTask(Task task, out bool isScheduleUpdated)
        {
            var appContext = new AppContext();
            var current = appContext.Context.Tasks.Find(task.Id);
            isScheduleUpdated = !current.Schedule.Equals(task.Schedule);
            EntityModelHelper.SetUpdatedValues(current, task, 
                "ResponsibleUserId", "Title", "Type", "Description", "Schedule", "Completed");
            appContext.Context.SaveChanges();
            return current;
        }
    }
}