﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Warehouse.Areas.DocStore
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles, string prefix)
        {
            bundles.Add(new ScriptBundle("~/bundles/DocStore").IncludeDirectory(
                $"{prefix}/DocStore/Scripts", "*.js", true));
        }
    }
}