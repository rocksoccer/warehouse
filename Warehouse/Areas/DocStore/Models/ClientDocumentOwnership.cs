﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Warehouse.Areas.DocStore.Libs;
using Warehouse.Models;

namespace Warehouse.Areas.DocStore.Models
{
    public class ClientDocumentOwnership : EntityBase
    {
        [Required]
        [AllowedValues(AllowedValues = "Client,Branch,User")]
        public string OwnerType { get; set; }
        [Required]
        public string OwnerId { get; set; }

        [Required]
        public int ClientId { get; set; }
        public virtual Client Client { get; set; }

        [Required]
        public int DocumentId { get; set; }
        public virtual Document Document { get; set; }
    }
}