﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Warehouse.Models;

namespace Warehouse.Areas.DocStore.Models
{
    public class ContractCategoryRelation : EntityBase
    {
        [Required]
        public int ContractId { get; set; }
        public virtual Contract Contract { get; set; }

        [Required]
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
    }
}