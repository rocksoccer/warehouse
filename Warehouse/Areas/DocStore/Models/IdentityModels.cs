﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Warehouse.Areas.DocStore.Models;

namespace Warehouse.Areas.DocStore.Models
{
    //public class IdentityModels
    //{
    //}
}

namespace Warehouse.Models
{
    public partial class ApplicationDbContext
    {
        public DbSet<ContractCategoryRelation> ContractCategoryRelations { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Subcategory> Subcategories { get; set; }
        public DbSet<ClientDocumentOwnership> ClientDocumentOwnerships { get; set; }
    }
}