﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Warehouse.Models;

namespace Warehouse.Areas.DocStore.Models
{
    public class Subcategory : EntityBase
    {
        [Required]
        [StringLength(200)]
        [Index("SubcategoryUnique", 1, IsUnique = true)]
        public string Name { get; set; }

        [Required]
        [Index("SubcategoryUnique", 2, IsUnique = true)]
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
        
        public virtual ICollection<Document> Documents { get; set; }
    }
}