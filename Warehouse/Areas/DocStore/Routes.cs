﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestfulRouting;
using Warehouse.Areas.DocStore.Controllers;

namespace Warehouse.Areas.DocStore
{
    public class Routes : RouteSet
    {
        public override void Map(IMapper map)
        {
            map.Area<DocumentController>("DocStore", "", area =>
            {
                area.Resources<DocumentTypeController>(documentType =>
                {
                    documentType.Only();
                    documentType.Resources<DocumentController>();
                });
            });
        }
    }
}