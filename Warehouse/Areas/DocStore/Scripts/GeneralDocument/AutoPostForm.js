﻿function AutoPostFormControl(target) {
    var self = this;
    this.target = $(target);
    this.form = this.target.closest("form");

    this.OnValueChange = function(event) {
        this.form.submit();
    };

    this.target.change(this.OnValueChange);
}

$(function () {
    var TYPES = ["select", "input"];
    var selectors = $.map(TYPES, function(value) {
        return value + "[data-auto-post-form='true']";
    });
    var selector = selectors.join(", ");
    $(selector).each(function (index, element) {
        new AutoPostFormControl(element);
    });
});