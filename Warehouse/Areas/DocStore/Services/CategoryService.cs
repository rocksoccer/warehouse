﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using Warehouse.App_Start;
using Warehouse.Areas.DocStore.Models;
using Warehouse.Libraries;
using Warehouse.Models;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Areas.DocStore.Services
{
    public interface ICategoryService
    {
        List<Category> AllCategories();

        List<Category> AllCategories(DocumentTypes documentType);
        Category AddCategory(Category category);
        Category GetCategory(int categoryId);
        Category UpdateCategory(Category category);

        List<Subcategory> AllSubcategories(DocumentTypes documentType);
        Subcategory AddSubcategory(int categoryId, Subcategory subcategory);
        Subcategory GetSubcategory(int subcategoryId);
        Subcategory UpdateSubcategory(Subcategory subcategory);
    }

    public class CategoryService : ICategoryService
    {
        public CategoryService()
        {
            _unityContainer = UnityConfig.GetConfiguredContainer();
        }

        private readonly IUnityContainer _unityContainer;

        #region Categories

        public List<Category> AllCategories()
        {
            var appContext = new AppContext();
            var generalCategories = Categories(DocumentTypes.General, appContext);
            var clientCategories = Categories(DocumentTypes.Client, appContext);
            var allCategories = generalCategories.Union(clientCategories);
            return allCategories.ToList();
        }

        public List<Category> AllCategories(DocumentTypes documentType)
        {
            var appContext = new AppContext();
            var categories = Categories(documentType, appContext);
            return categories.ToList();
        }

        public Category AddCategory(Category category)
        {
            var appContext = new AppContext();
            var saved = appContext.Context.Categories.Add(category);
            appContext.Context.SaveChanges();
            return saved;
        }

        public Category GetCategory(int categoryId)
        {
            var appContext = new AppContext();
            var category = appContext.Context.Categories.Find(categoryId);
            return category;
        }

        public Category UpdateCategory(Category category)
        {
            var appContext = new AppContext();
            var currentCategory = appContext.Context.Categories.Find(category.Id);
            EntityModelHelper.SetUpdatedValues(currentCategory, category, "Name", "DocumentType");
            appContext.Context.SaveChanges();
            return currentCategory;
        }

        #endregion

        #region subcategories

        public List<Subcategory> AllSubcategories(DocumentTypes documentType)
        {
            var appContext = new AppContext();
            var categories = Categories(documentType, appContext);
            var subcategories = categories.SelectMany(c => c.Subcategories).ToList();
            return subcategories;
        }

        public Subcategory AddSubcategory(int categoryId, Subcategory subcategory)
        {
            var appContext = new AppContext();
            var category = appContext.Context.Categories.Find(categoryId);
            category.Subcategories.Add(subcategory);
            appContext.Context.SaveChanges();
            return subcategory;
        }

        public Subcategory GetSubcategory(int subcategoryId)
        {
            var appContext = new AppContext();
            var subcategory = appContext.Context.Subcategories.Find(subcategoryId);
            return subcategory;
        }

        public Subcategory UpdateSubcategory(Subcategory subcategory)
        {
            var appContext = new AppContext();
            var currentSubcategory = appContext.Context.Subcategories.Find(subcategory.Id);
            EntityModelHelper.SetUpdatedValues(currentSubcategory, subcategory, "Name");
            appContext.Context.SaveChanges();
            return currentSubcategory;
        }

        #endregion

        private IQueryable<Category> Categories(DocumentTypes documentType, AppContext appContext)
        {
            var categoryProvider = CategoryProvider(documentType, appContext);
            return categoryProvider.Categories(appContext);
        }

        private ICategoryProviderService CategoryProvider(DocumentTypes documentType, AppContext appContext)
        {
            var namePrefix = appContext.IsCurrentUserAdmin ? "Admin" : "Client";
            var providerName = $"{documentType}CategoryProvider{namePrefix}Service";
            var provider = _unityContainer.Resolve<ICategoryProviderService>(providerName);
            return provider;
        }
    }
}