﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Areas.DocStore.Models;
using Warehouse.Libraries;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Areas.DocStore.Services
{
    public class ClientCategoryProviderAdminService : ICategoryProviderService
    {
        private readonly string _clientDocumentType = DocumentTypes.Client.ToString();

        public IQueryable<Category> Categories(AppContext appContext)
        {
            return appContext.Context.Categories.Where(c => c.DocumentType == _clientDocumentType);
        }
    }
}