﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Warehouse.Areas.DocStore.Models;
using AppContext = Warehouse.Libraries.AppContext;

namespace Warehouse.Areas.DocStore.Services
{
    public class GeneralCategoryProviderClientService : ICategoryProviderService
    {
        private readonly string _generalDocumentType = DocumentTypes.General.ToString();

        public IQueryable<Category> Categories(AppContext appContext)
        {
            var context = appContext.Context;
            var user = appContext.CurrentUser;

            var contractIds = user.Client.Contracts.Select(c => c.Id).ToList(); //TODO: check for contract expiration
            var categories = context.ContractCategoryRelations
                .Where(r => contractIds.Contains(r.ContractId))
                .Select(r => r.Category);
            return categories.Where(c => c.DocumentType == _generalDocumentType);
        }
    }
}