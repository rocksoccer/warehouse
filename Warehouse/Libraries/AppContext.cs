﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Warehouse.Libraries.UsersManagement;
using Warehouse.Models;

namespace Warehouse.Libraries
{
    public class AppContext
    {
        public AppContext()
        {
            Context = new ApplicationDbContext();
        }

        public ApplicationDbContext Context { get; }

        private ApplicationUser _currentUser;
        public ApplicationUser CurrentUser => _currentUser ?? (_currentUser = Context.Users.Find(HttpContext.Current.User.Identity.GetUserId()));

        private bool _isCurrentUserAdminChecked;
        private bool _isCurrentUserAdmin;
        public bool IsCurrentUserAdmin
        {
            get
            {
                if (CurrentUser == null) return false;
                if (_isCurrentUserAdminChecked) return _isCurrentUserAdmin;
                
                _isCurrentUserAdmin = CurrentUserRoles().Contains(ApplicationRoleTypes.Admin.ToString());
                _isCurrentUserAdminChecked = true;
                return _isCurrentUserAdmin;
            }
        }

        private bool _isCurrentUserClientManagerChecked;
        private bool _isCurrentUserClientManager;
        public bool IsCurrentUserClientManager
        {
            get
            {
                if (CurrentUser == null) return false;
                if (_isCurrentUserClientManagerChecked) return _isCurrentUserClientManager;

                var roles = CurrentUserRoles();
                _isCurrentUserClientManager = roles.Contains(ApplicationRoleTypes.Client.ToString()) && roles.Contains(ApplicationRolePositions.Manager.ToString());
                _isCurrentUserClientManagerChecked = true;
                return _isCurrentUserClientManager;
            }
        }

        private IList<string> CurrentUserRoles()
        {
            var store = new UserStore<ApplicationUser>(Context);
            var manager = new UserManager<ApplicationUser>(store);
            return manager.GetRoles(CurrentUser.Id);
        }
    }
}