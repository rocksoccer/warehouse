﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Warehouse.Libraries.UsersManagement
{
    public enum ApplicationRoleTypes
    {
        Client,
        Admin
    }
    public enum ApplicationRolePositions
    {
        User,
        Manager,
    }

    public class ApplicationRole
    {
        //DO NOT USE these, they are just for controllers authorize attributes
        public const string Client = "Client";
        public const string Admin = "Admin";
        public const string PositionUser = "User";
        public const string PositionManager = "Manager";

        private const string RoleStringSplitter = "::";

        public static ApplicationRole ParseRoleString(string roleString)
        {
            var splitted = roleString.Split(new[] { RoleStringSplitter }, StringSplitOptions.None);
            var type = (ApplicationRoleTypes)Enum.Parse(typeof(ApplicationRoleTypes), splitted[0], true);
            var position = (ApplicationRolePositions)Enum.Parse(typeof(ApplicationRolePositions), splitted[1], true);
            return new ApplicationRole(type, position);
        }

        public ApplicationRole(ApplicationRoleTypes type, ApplicationRolePositions position)
        {
            Type = type;
            Position = position;
        }

        public ApplicationRoleTypes Type { get; }
        public ApplicationRolePositions Position { get; }
        public string[] RolesAsStrings => new[] { Type.ToString(), Position.ToString() };

        public override string ToString()
        {
            return $"{Type}{RoleStringSplitter}{Position}";
        }
    }
}